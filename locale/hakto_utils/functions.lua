-- Add own styles and override oarcs stylings

server_settings_style = {
    minimal_width = 450,
    maximal_height = 15,
    font = "default-semibold",
    font_color = {r=40,g=180,b=255}
}

server_settings_style_s = {
    minimal_width = 450,
    maximal_height = 10,
    font = "default-small-semibold",
    font_color = {r=40,g=180,b=255}
}

message_style_s = {
    minimal_width = 450,
    maximal_height = 10,
    font = "default-small-semibold"
}

my_warning_style = {
    minimal_width = 450,
    maximal_width = 450,
    maximal_height = 10,
    font = "default-semibold",
    font_color = {r=1,g=0.4,b=0}
}

my_spacer_style = {
    minimal_width = 450,
    maximal_width = 450,
    minimal_height = 10,
    maximal_height = 10,
    font_color = {r=0,g=0,b=0}
}

-- Misc

-- Notify Admin Event
function NotifyAdmin(message)
    for _, player in pairs(game.players) do 
        if player.admin then
            player.print(message)
        end
    end
end

function ConvertHours (tick)
    local hour = tostring(math.floor(tick * (1 /(60*game.speed)) / 3600))
    return hour
end

function ConvertMinutes (tick)
    local minutes = math.floor((tick * (1 /(60*game.speed))) / 60)
    return minutes
end

function SetHostServerMessages(event)
    local player = game.players[event.player_index]

    global.welcome_msg = WELCOME_MSG_HEAD .. global.serverHost .. WELCOME_MSG_TAIL
    global.welcome_msg_title = WELCOME_MSG_HEAD .. global.serverHost .. WELCOME_MSG_TAIL
    
    if global.serverHost ~= nil then
        if global.serverHost == "Hakto" then
            MODIFIED_INFO1 = "Join the Discord channel of this server: qqBfyMF"
            MODIFIED_INFO2 = "Server: OVH dedicated, Xeon 1225v2, 32GB RAM, 250Mbit Uplink"
            GAME_NAME = "Game name: [EU] Hakto's Server (Oarc Scenario | 06.03.)"
        end
    end
end

-- Workaround to identify the host for custom messages
-- somehow players[1] is empty you dont stay long enough after the init join

function LogHost(player)
    if global.serverHost == nil then        
        global.serverHost = player.name
    end
end

-- Allow player to have an larger inventory
function GivePlayerLargerInventory(player)
    if player ~= nil then
        player.character.character_inventory_slots_bonus = INVENTORY_BONUS
    end
end

-- Raise the movementspeed of the player
function GivePlayerFasterMovement(player)
    if player ~= nil then
        player.character.character_running_speed_modifier = MOVESPEED_BONUS
    end
end

-- Kickstart
function EnableAutomatedConstruction(force)
    force.technologies['automated-construction'].researched=true;
end

function EnableEfficientRobots(force)
    force.worker_robots_storage_bonus = EFFICIENT_BOTS_CAPACITYBONUS;
    force.worker_robots_speed_modifier = EFFICIENT_BOTS_SPEEDBONUS;
end

-- Give player these default items.
function GivePlayerItems(player)
    player.insert{name="steel-axe", count = 5}
    player.insert{name="pistol", count=1}
    player.insert{name="firearm-magazine", count=100}
end

-- Additional Kickstart items
function GivePlayerStarterItems(player)
    GivePlayerItems(player)
    if ENABLE_KICKSTART then
        player.insert{name="steel-axe", count = 5}
        player.insert{name="lab", count=1}
        player.insert{name="science-pack-1", count=10}
        player.insert{name="burner-mining-drill", count = 10}
        player.insert{name="stone-furnace", count = 10}
        player.insert{name="power-armor", count = 1}
        player.insert{name="fusion-reactor-equipment", count = 1}
        player.insert{name="personal-roboport-equipment", count = 2}
        player.insert{name="construction-robot", count = 20}
        player.insert{name="battery-mk2-equipment", count = 1}
        player.insert{name="blueprint", count = 3}
        player.insert{name="deconstruction-planner", count = 1}
    else
        player.insert{name="burner-mining-drill", count = 3}
        player.insert{name="stone-furnace", count = 1}
    end
end

-- Playerlisting

playerlist_host = {
    minimal_width = 160,
    maximal_width = 160,
    maximal_height = 12,
    font_color = {r=255,g=0,b=0}
}

playerlist_admin = {
    minimal_width = 160,
    maximal_width = 160,
    maximal_height = 12,
    font_color = {r=255,g=60,b=0}
}

playerlist_8h = {
    minimal_width = 160,
    maximal_width = 160,
    maximal_height = 12,
    font_color = {r=255,g=140,b=0}
}

playerlist_4h = {
    minimal_width = 160,
    maximal_width = 160,
    maximal_height = 12,
    font_color = {r=255,g=180,b=0}
}

playerlist_2h = {
    minimal_width = 160,
    maximal_width = 160,
    maximal_height = 12,
    font_color = {r=255,g=220,b=0}
}

playerlist_0h = {
    minimal_width = 160,
    maximal_width = 160,
    maximal_height = 12,
    font_color = {r=255,g=255,b=255}
}

playerlist_spacer = {
    minimal_width = 160,
    maximal_width = 160,
    minimal_height = 5,
    maximal_height = 5,
    font_color = {r=0,g=0,b=0}
}

function CreatePlayerListGui(event)
    local player = game.players[event.player_index]
    if player.gui.top.playerlistbutton == nil then
        player.gui.top.add{name="playerlistbutton", type="button", caption="Players", tooltip= "Display Active Players"}
    end   
end

function PlayerListGuiClick(event) 
    if not (event and event.element and event.element.valid) then return end
    local player = game.players[event.element.player_index]
    local name = event.element.name
    
    if (name == "playerlistbutton") then
        local frame = player.gui.left["PlayerListFrame"]
        if (frame) then
            frame.destroy()
        else
            player.gui.left.add{name= "PlayerListFrame", type = "frame", direction = "vertical", caption = "Active Players"}
            local playerListGui = player.gui.left.PlayerListFrame
            
            for key, player in pairs(game.connected_players) do
                if player ~= nil and player.valid then
                    
                    local PTMinutes = ConvertMinutes(player.online_time)
                    local PTHours = ConvertHours(player.online_time)
                    
                    if player.admin == true then
                        -- global.serverHost will be the name of the save creator
                        if (global.serverHost == player.name) then
                            playerListGui.add{type = "label",  name = player.name, style="caption_label_style", caption={"", player.name, " (", PTHours, "h, Host)"}}
                            ApplyStyle(playerListGui[player.name], playerlist_host)
                        else
                            playerListGui.add{type = "label",  name = player.name, style="caption_label_style", caption={"", player.name, " (", PTHours, "h, Admin)"}}
                            ApplyStyle(playerListGui[player.name], playerlist_admin)
                        end
                    else
                        playerListGui.add{type = "label",  name = player.name, style="caption_label_style", caption={"", player.name, " (", PTHours, "h)",}}
                        
                        if PTMinutes >= 480 then
                            ApplyStyle(playerListGui[player.name], playerlist_8h)
                        elseif PTMinutes >= 240 then
                            ApplyStyle(playerListGui[player.name], playerlist_4h)
                        elseif PTMinutes >= 120 then
                            ApplyStyle(playerListGui[player.name], playerlist_2h)
                        else
                            ApplyStyle(playerListGui[player.name], playerlist_0h)
                        end
                    end
                end
            end
            
            playerListGui.add{name = "guispacer", type = "label", caption=" "}
            ApplyStyle(playerListGui.guispacer, playerlist_spacer)
        end
    end    
end

-- Server Rules

function CreateRuleGui(event)
    local player = game.players[event.player_index]
    if player.gui.top.serverrule_btn == nil then
        player.gui.top.add{name="serverrule_btn", type="button", caption="Info", tooltip= "Rules & Infos"}
    end   
end

rules_label_style = {
    minimal_width = 400,
    maximal_width = 400,
    maximal_height = 12,
    font_color = {r=1,g=1,b=1}
}

function RuleGuiClick(event) 
    if not (event and event.element and event.element.valid) then return end
    local player = game.players[event.element.player_index]
    local name = event.element.name
    
    if (name == "serverrule_btn") then
        local frame = player.gui.center["ruleframe"]
        if (frame) then
            frame.destroy()
        else
            player.gui.center.add{name = "ruleframe",type = "frame",direction = "vertical",caption=RULEHEADLINE}
            
            local ruleGui = player.gui.center.ruleframe
            ruleGui.add{name = "rule1", type = "label", caption=RULE1}
            ruleGui.add{name = "rule2", type = "label", caption=RULE2}
            ruleGui.add{name = "rule3", type = "label", caption=RULE3}
            ruleGui.add{name = "rule4", type = "label", caption=RULE4}
            ruleGui.add{name = "rule5", type = "label", caption=RULE5}
            ruleGui.add{name = "rule6", type = "label", caption=RULE6}
            ruleGui.add{name = "rule7", type = "label", caption=RULE7}
            
            ApplyStyle(ruleGui.rule1, rules_label_style)
            ApplyStyle(ruleGui.rule2, rules_label_style)
            ApplyStyle(ruleGui.rule3, rules_label_style)
            ApplyStyle(ruleGui.rule4, rules_label_style)
            ApplyStyle(ruleGui.rule5, rules_label_style)
            ApplyStyle(ruleGui.rule6, rules_label_style)
            ApplyStyle(ruleGui.rule1, rules_label_style)
            
            ruleGui.add{name = "rulespacer1", type = "label", caption=" "}            
            ruleGui.add{name = "rulegamename", type = "label", caption= GAME_NAME }            
            ruleGui.add{name = "rulegamediscord", type = "label", caption= MODIFIED_INFO1 }            
            ruleGui.add{name = "rulespacer2", type = "label", caption=" "}
            
            ApplyStyle(ruleGui.rulegamename, rules_label_style)
            ApplyStyle(ruleGui.rulegamediscord, rules_label_style)
            ApplyStyle(ruleGui.rulespacer1, my_spacer_style)
            ApplyStyle(ruleGui.rulespacer2, my_spacer_style)
                        
            ruleGui.add{name = "rulegameserver", type = "label", caption= MODIFIED_INFO2 }            
            ruleGui.add{name = "rulespacer3", type = "label", caption=" "}
            
            ApplyStyle(ruleGui.rulegameserver, rules_label_style)
            ApplyStyle(ruleGui.rulespacer3, my_spacer_style)

            ruleGui.add{name = "rulecredits1", type = "label", caption= "Credits to all mods/plugin authors:" }
            ruleGui.add{name = "rulecredits2", type = "label", caption= "Oarc, Orzelek, 3RA Gaming, DaveMcW & more." }   
            ruleGui.add{name = "rulespacer4", type = "label", caption=" "}

            ApplyStyle(ruleGui.rulecredits1, rules_label_style)
            ApplyStyle(ruleGui.rulecredits2, rules_label_style)
            ApplyStyle(ruleGui.rulespacer4, my_spacer_style)
        end
    end    
end

-- Add autofill to furnaces and basic drills (override oarcs version)

-- Autofills a smelter or drill
function AutoFillOven(player, oven)
    local mainInv = player.get_inventory(defines.inventory.player_main)
    
    -- Attempt to transfer some coal
    if ((oven.name == "burner-mining-drill") or (oven.name == "stone-furnace")) then
        TransferItemMultipleTypes(mainInv, oven, {"coal"}, 10)
    end
end

function Autofill(event)
    local player = game.players[event.player_index]
    local eventEntity = event.created_entity
    
    if (eventEntity.name == "gun-turret") then
        AutofillTurret(player, eventEntity)
    end
    
    if ((eventEntity.name == "car") or (eventEntity.name == "tank") or (eventEntity.name == "diesel-locomotive")) then
        AutoFillVehicle(player, eventEntity)
    end
    
    if ((eventEntity.name == "burner-mining-drill") or (eventEntity.name == "stone-furnace")) then
        AutoFillOven(player, eventEntity)
    end
end