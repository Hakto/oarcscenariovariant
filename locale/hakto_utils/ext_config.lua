--------------------------------------------------------------------------------
-- Modified Stuff
--------------------------------------------------------------------------------

WELCOME_MSG_HEAD = "Welcome to "
WELCOME_MSG_TAIL = "'s Server!"

WELCOME_MSG1 = "This server is running Oarc's PvE Wilderness Spawn Scenario."
WELCOME_MSG2 = "You spawn on diffrent locations and have to reach the silo."
WELCOME_MSG3 = "This version has some comfort bonuses for the early game."
WELCOME_MSG4 = "Read the Info & Rules on the top left corner and have fun!"

-- Display game name in info gui (many asking the name before reconnecting)
GAME_NAME = "Game name: n/a"

MODIFIED_INFO1 = "Join the Discord channel of this server: n/a"
MODIFIED_INFO2 = "Server: n/a"

-- Rules
RULEHEADLINE = "Server Rules"
RULE1 = "1. Be polite and respectful to all players."
RULE2 = "2. Ask the owner of the base, before you change anything."
RULE3 = "3. If you join a base, don't criticize their builds."
RULE4 = "4. LHD (signals inside) preferred, but not mandatory :)"
RULE5 = "5. Do NOT concrete/pave with bots - NEVER!"
RULE6 = "6. Don't complain about lags. 99% it's your CPU or location."
RULE7 = "7. Do not grief, cheat, glitch or exploit. Ban inc.!"

-- Version changes
VERSION_MSG1 = "Version (0.2.9.1):"
VERSION_MSG2 = "Possible bugfix for desync loop while pressing INFO."

--------------------------------------------------------------------------------
-- Additional mod config
--------------------------------------------------------------------------------

-- Enable Rules GUI
ENABLE_RULES = true

-- Enable Bots on game start (set items in hakto_utils)
ENABLE_KICKSTART = false

-- Enable Playerlist
ENABLE_PLAYERLIST = true

-- Enable Larger Inventory
ENABLE_LARGERINVENTORY = true
INVENTORY_BONUS = 20

-- Enable Faster Movement
ENABLE_FASTERMOVEMENT = true
MOVESPEED_BONUS = 0.2

-- Faster Bots and higher capacity to save botusage and prob lag cause
ENABLE_EFFICIENT_BOTS = true
EFFICIENT_BOTS_SPEEDBONUS = 0.2
EFFICIENT_BOTS_CAPACITYBONUS = 3

---------------------------------------
-- Oarc default settings override
---------------------------------------
SILO_CHUNK_DISTANCE_X = 280

-- Enemy settings
ENEMY_EXPANSION = false
ENEMY_POLLUTION_FACTOR_DIVISOR = 6
ENEMY_DESTROY_FACTOR_DIVISOR = 4

-- Long Reach modifier
BUILD_DIST_BONUS = 16
REACH_DIST_BONUS = BUILD_DIST_BONUS

-- Safe area
SAFE_AREA_CHUNK_MULT = 8
SAFE_AREA_TILE_DIST = CHUNK_SIZE*SAFE_AREA_CHUNK_MULT

-- Start resource amounts
START_STONE_AMOUNT = 2000
START_IRON_AMOUNT = 2000
START_COPPER_AMOUNT = 2000
START_COAL_AMOUNT = 2000
START_OIL_AMOUNT = 22500

WATER_SPAWN_OFFSET_X = -6
WATER_SPAWN_OFFSET_Y = -38
WATER_SPAWN_LENGTH = 12

START_RESOURCE_OIL_POS_X = 14
START_RESOURCE_OIL_POS_Y = -38

RESPAWN_COOLDOWN_IN_MINUTES = 20
RESPAWN_COOLDOWN_TICKS = TICKS_PER_MINUTE * RESPAWN_COOLDOWN_IN_MINUTES

-- Require playes to be online for at least 5 minutes
-- Else their character is removed and their spawn point is freed up for use
MIN_ONLIME_TIME_IN_MINUTES = 5
MIN_ONLINE_TIME = TICKS_PER_MINUTE * MIN_ONLIME_TIME_IN_MINUTES