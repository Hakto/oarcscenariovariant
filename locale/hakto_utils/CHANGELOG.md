# Hakto's Oarc Scenario Variant - Change Log

## [Unreleased]
### In Progress

### To do
- Add invite function to a base owner
- Add kick function (remove players which are not on the team list of a base)
- Add kill, mass deconstruction and aoe weapon usage blockade for players under 30 minute playtime
- Make use of the admin notification for the functons above
- Add a function to voteban a player to a seperate generated isle (only players which are 1+ h on the server can vote)
- Change rules to an array + function to allow users add/change rules more quickly

## (0.2.9.1) - 2017-03-07
### Changed
- Added versioning and changelog
- Code cleanup and seperation for upcoming git repository release

### Bugfix
- Added a possible fix to a new mass desync by gui clicking
- Fixed a bug where players get admin = 1 on joining, by pressing the players gui

## 2017-02-24
### Bugfix
- Fixed the host identification and seperated it (for wlc msg and player list)

### Added
- Added custom messages to allow users hosting it without hardcoded names
- Welcome message gui now shows the latest changes

## 2017-02-20
### Bugfix
- Added the fix by Oarc and removed the workaround.

## 2017-02-19
### Bugfix
- Added a workaround to the starter resource generation to prevent 4x generation bug

## 2017-02-16
### Changed
- Removed wood from autofill for drills and furnaces

## 2017-02-12
### Added
- Added autofill to drills and furnaces
- Added bot carry size and movement speed config modifier

### Changed
- Removed admin GUI (too many mass desync triggers on corrupt table)

## 2017-02-07
### Changed
- Changed welcome message gui colors and layout

### Added
- Added the scenario settings to the initial display, to show the player the actual setting
- Added rule & info gui

## 2017-01-22
### Changed
- Cleaned up the player list function

### Bugfix
- Fixed mass desync bug on the player list function

## 2017-01-08
### Added
- Added movement speed config modifier
- Added inventory size config modifier
- Added initial player list function (inspired by badgamernl)
- Added admin GUI (inspired by jvmguy)
- Added kickstart package option (inspired by jvmguy)