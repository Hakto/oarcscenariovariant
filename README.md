# Hakto's variant of Oarc Wilderness Spawn Scenario

This is a slightly tweaked version of Oarc's Wilderness Spawn Scenario.

I asked for Oarcs permission to do so.

The reason was to speed up the beginner phase in a more comfortable way.

Feel free to report bugs or contact me on discord (Hakto#9472).

You can find the original version by Oarcinae here:

https://github.com/Oarcinae/FactorioScenarioMultiplayerSpawn

Changes

- Additional Movespeed modifier
- Additional Character Inventar Slot modifier
- Faster Bots with larger cargo size modifier
- Kickstart option (inspired by jvmguys variant)
- Playerlisting GUI to show played time and admins
- Rules GUI
- Restyled Infoscreen

## Instructions

### STEP 1

Go to Downloads (left side) and then select Branches to download the version you like,
by clicking the file format on the right side.

Place it in your Factorio/scenarios/... folder.

It should look something like this (for my windows steam install location):

C:\Users\user\AppData\Roaming\Factorio\scenarios\Hakto-oarcscenariovariant\control.lua

### STEP 2

Go into hakto_utils\ext_config.lua or config.lua and edit the strings to add your own server messages or modify the scenario settings.

### STEP 3

To host a game, create a new game via scenario on you pc and set resources and aliens to "none".

Then save it and upload the savegame to your server or open it as a new multiplayer game,
if you host it local as a non-headless game.

Be sure to edit the ruletable and config to you needs.

## Credits

Credits to the author of every mod which is included here.